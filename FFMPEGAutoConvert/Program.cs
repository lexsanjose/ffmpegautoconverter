﻿using System;
using System.IO;
using System.Diagnostics;
using System.Configuration;
using System.Globalization;

namespace FFMPEGAutoConvert
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = ConfigurationManager.AppSettings["input"];
            string output = ConfigurationManager.AppSettings["output"];
            string res = ConfigurationManager.AppSettings["resolution"];
            bool createNoWindow = Convert.ToBoolean(ConfigurationManager.AppSettings["createNoWindow"]);
            string bat_params = @"CD bin
                                SET PATH=%CD%;%PATH%
                                ffmpeg -i " + "\""+"{0}"+"\""+" -c:v libx264 -c:a copy -c:s copy -s {1} -preset medium -crf 23.976 "+"\""+"{2}"+"\"";

            Console.WriteLine("REF 1: http://manpages.ubuntu.com/manpages/karmic/en/man1/ffmpeg.1.html");
            Console.WriteLine("REF 2: https://trac.ffmpeg.org/wiki/Encode/H.264");

            if (!Directory.Exists(output))
                Directory.CreateDirectory(output);

            Console.WriteLine("\nOutput Folder: {0}", output);
            Console.WriteLine("Resolution: {0}", res);

            string bat = Path.Combine(Directory.GetCurrentDirectory(), "auto.bat");
            foreach(string file in Directory.GetFiles(input))
            {
                //create date folder on where to move the output file
                string dateFilePath = DateTime.ParseExact(Path.GetFileName(file).Split('_')[1], "yyyyMMdd", CultureInfo.InvariantCulture).ToString("MMMM d");
                dateFilePath = Path.Combine(output, dateFilePath);
                
                if (!Directory.Exists(dateFilePath))
                    Directory.CreateDirectory(dateFilePath);

                string outputFile = $"{dateFilePath}\\{Path.GetFileName(file)}";

                Console.Write("Processing: " + Path.GetFileName(file) + " | ");
                using (StreamWriter sw = new StreamWriter(bat))
                    sw.WriteLine(string.Format(bat_params, file, res, outputFile));

                ProcessStartInfo StartInfo = new ProcessStartInfo(bat);
                //StartInfo.RedirectStandardOutput = true;
                //StartInfo.UseShellExecute = false;
                StartInfo.WindowStyle = createNoWindow ? ProcessWindowStyle.Hidden : ProcessWindowStyle.Normal;
                Process Proc = Process.Start(StartInfo);
                Proc.WaitForExit();
                Console.WriteLine("Done!");
            }

            //Append day N for each file
            //string[] outputDirectories = Directory.GetDirectories(output);
            //for (int i = 0; i < outputDirectories.Length; i++)
            //    Directory.Move(outputDirectories[i], $"{outputDirectories[i]} - Day {i+1}");

            Console.WriteLine("Completed!");
            Console.ReadLine();
        }
    }
}
