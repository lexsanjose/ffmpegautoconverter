# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

1. Aims to convert high-res vidoes to low-res to be able to play videos in mobile devices
2. Allows conversion of multiple files inside the specified folder

### How do I get set up? ###

1. Download bin/Debug
2. Run FFMPEGAutoConvert.exe
3. Follow instructions on the UI

### Contribution guidelines ###

1. Uses ffmpeg (https://www.ffmpeg.org/)
